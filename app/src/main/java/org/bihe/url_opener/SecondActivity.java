package org.bihe.url_opener;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

public class SecondActivity extends AppCompatActivity {
    public static final String EXTRA_URL = "url";
    private EditText mURLTextInput;
    private Button mConfirmBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        mURLTextInput = findViewById(R.id.url_text_input);
        mConfirmBtn = findViewById(R.id.confirm_button);

    }

    public void confirm (View view){
        String url = mURLTextInput.getText().toString();
        if (url.startsWith("http://") || url.startsWith("https://"))
        {
            Intent intent = new Intent();
            intent.putExtra(EXTRA_URL, url);
            setResult(RESULT_OK, intent);
            finish();
        }
        Toast.makeText(getApplicationContext(), "Please type a valid URL", Toast.LENGTH_LONG).show();
    }
}