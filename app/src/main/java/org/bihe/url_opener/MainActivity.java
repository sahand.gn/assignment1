package org.bihe.url_opener;


import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private static final int URL_REQUEST = 1;

    private TextView mUrlTextView;
    private Button mOpenButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mUrlTextView = findViewById(R.id.url_text_view);
        mOpenButton = findViewById(R.id.url_open_btn);
    }

    public void sendUrl (View view){
        Intent intent = new Intent(getApplicationContext(),  SecondActivity.class);
        startActivityForResult(intent,URL_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == URL_REQUEST){
            if (resultCode == RESULT_OK){
                String url = data.getStringExtra(SecondActivity.EXTRA_URL);
                mUrlTextView.setVisibility(View.VISIBLE);
                mOpenButton.setVisibility(View.VISIBLE);
                mUrlTextView.setText(url);
            }
        }
    }

    public void open_website (View view){
        String url = mUrlTextView.getText().toString();
        if (!url.startsWith("http://") && !url.startsWith("https://"))
        {
            url = "http://" + url;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }
}